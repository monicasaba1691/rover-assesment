<?php 
	function process_instructions($exp_instructions, $curent_pos_x, $curent_pos_y, $curent_pos_f, $upper_right){
		$exp_instructions = str_split($exp_instructions);
		foreach ($exp_instructions as $instruction) {
			if ($instruction == 'L'){
				switch ($curent_pos_f) {
			    case 'N':
			        $curent_pos_f = 'W';
			        break;
			    case 'W':
			        $curent_pos_f = 'S';
			        break;
			    case 'S':
			        $curent_pos_f = 'E';
			        break;
		        case 'E':
			        $curent_pos_f = 'N';
			        break;
				}
			}
			else if($instruction == 'R'){
				switch ($curent_pos_f) {
			    case 'N':
			        $curent_pos_f = 'E';
			        break;
			    case 'E':
			        $curent_pos_f = 'S';
			        break;
			    case 'S':
			        $curent_pos_f = 'W';
			        break;
		        case 'W':
			        $curent_pos_f = 'N';
			        break;
				}
			}
			else if ($instruction == 'M'){
				switch ($curent_pos_f) {
			    case 'N':
			    	if ($upper_right['upper_y'] < ($curent_pos_y + 1)){
			    		echo 'You are outside the border.';
			    	}
			    	else{
			    		$curent_pos_y += 1;
			    	}
			        break;
			    case 'E':
			    	if ($upper_right['upper_x'] < ($curent_pos_x + 1)){
			    		echo 'You are outside the border.';
			    	}
			    	else{
			    		$curent_pos_x += 1;
			    	}
			        break;
			    case 'S':
			    	if (($curent_pos_y - 1) < 0){
			    		echo 'You are outside the border.';
			    	}
			    	else{
			    		$curent_pos_y -= 1;
			    	}
			        break;
		        case 'W':
			        if (($curent_pos_x - 1) < 0){
			    		echo 'You are outside the border.';
			    	}
			    	else{
			    		$curent_pos_x -= 1;
			    	}
			        break;
				}
			}
			else {
				echo 'You gave wrong instructions.';
			}
		}

		$output['final_x'] = $curent_pos_x;
		$output['final_y'] = $curent_pos_y;
		$output['final_f'] = $curent_pos_f;

		return $output;
	}

	$my_upper_right['upper_x'] = 5;
	$my_upper_right['upper_y'] = 5;

	$rover_position1['position_x'] = 1;
	$rover_position1['position_y'] = 2;
	$rover_position1['position_f'] = 'N';

	$rover_position2['position_x'] = 3;
	$rover_position2['position_y'] = 3;
	$rover_position2['position_f'] = 'E';

	$exp_instructions1 = 'LMLMLMLMM';
	$exp_instructions2 = 'MMRMMRMRRM';

	$rover1_output = process_instructions($exp_instructions1, $rover_position1['position_x'], $rover_position1['position_y'], $rover_position1['position_f'], $my_upper_right);
	$rover2_output = process_instructions($exp_instructions2, $rover_position2['position_x'], $rover_position2['position_y'], $rover_position2['position_f'], $my_upper_right);

	echo 'First Rover ';
	echo '<pre>'; print_r($rover1_output); echo '</pre>';
	echo 'Second Rover ';
	echo '<pre>'; print_r($rover2_output); echo '</pre>';

?>